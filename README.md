# Lidl_k8s

### Create a production ready kubernetes cluster


This task was solved totally, I used digital ocean, becouse it has a nice python API, I used it in the jupyter notebook.Files for this task are in the `kube_cluster` folder
To recreate my steps open the `LIDL.ipynb` (I also exported the notebook as html to view it easly) with juypter notebook, set the two needed variable (`personal_token` and `path_to_project`) at the top, and run the cells.
(I also exported the notebook as html to view it easly)
(You can create your personal token in your account settings in digital ocean's website)
It will render a hosts file for ansible. I created for 4 ansible playbooks to create the cluster on the virtual machines:
Specfiy your secret key you already set in the commands after the `--private-key` flag
Install ansible on your local machine if needed (`brew/apt/yum install ansible`...)

`ansible-playbook -i hosts start.yaml --private-key ~/.ssh/sshkey_to_do`

`ansible-playbook -i hosts k8s_dep.yaml --private-key ~/.ssh/sshkey_to_do`

`ansible-playbook -i hosts master.yaml --private-key ~/.ssh/sshkey_to_do`

`ansible-playbook -i hosts workers.yaml --private-key ~/.ssh/sshkey_to_do`

Install ansible on your local machine if needed (`brew/apt/yum install ansible`...)

The cluster is ready!! You can test it by running `kubectl get nodes` on the master node
### Deploy a database on the cluster

Files for this task are in the `kube_application` folder
Copy this folder to the master node (ip can be found in output of  ipython cells or the rendered `hosts` file)

`scp i ~/.ssh/sshkey_to_do  kubeapplication/mysql.yaml ubuntu@MASTER_IP:/~`

`scp i ~/.ssh/sshkey_to_do  kubeapplication/persistent_volume.yaml ubuntu@MASTER_IP:/~`

Ssh to the master node and run there these commands:
`kubectl apply -f ~/persistent_volume.yaml `

`kubectl apply -f ~/mysql.yaml `

The database is up and running. Some sanity checks:

`kubectl describe pvc mysql-pv-claim`

`kubectl get pods -l app=mysql`

`kubectl describe deployment mysql`

`kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -ppassword `


### Deploy Airflow on the cluster

This task was not done :( 
It was hard for me at firt to grasp what Airflow is really about, I had to watch 2-3 tutorials, guides and read some articles.
I was not sure is it a python library, a framework, a nice UI or what :D. Every tutorial started with pip installing it and building the container...
Anyway.
I installed helm, added a couple of repos

`curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash`

`helm repo add stable https://kubernetes-charts.storage.googleapis.com`

`helm repo add bitnami https://charts.bitnami.com/bitnami`

Then tried to found a good airflow package, the best one was the stable/airflow, version 6.10.4
`helm install "airflow" stable/airflow`
This resulted in restarting, and crashlooping containers. airflow-web, and airflow-sheduler were the problematic pods.
I mainly worked based on this: https://hub.helm.sh/charts/stable/airflow
I tried to finetune the chart, using custom values. There was a lot of options I was unable to figure out the most important ones. I uploaded the my final version (`values.yaml`) 
Then my time was up :D

### My thoughts

It was a fine task. Learned some stuff about airflow. Seems like a nice tool.


### Next stepts

* Harden the kluster, create namespaces, users, serviceaccounts, rolebasedbindings ans such things.
* Add firewall to the virtaul machines. 
* Make the cluster accessible with kubectl from outside, deploy k8s dashboard
* Make airflow to work...
* Make this readme nicer

